package gitdeploytool;

import java.io.File;
import java.io.FileInputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.util.Base64;

/**
 *
 * @author eliotm
 */
public class LTBFile {

    public final static String SHA1 = "SHA-1";
    public final static String MD5 = "MD5";

    private final Ref ref;
    private final String modType;
    private final String path;
    private String hashKey;
    private ObjectLoader fileLoader;
    private boolean isGITFile;
    private String origGitPath;
    private long fileLength;

    public LTBFile(Ref rc, String mt, String p) {
        ref = rc;
        modType = mt;
        path = p;
        hashKey = "NOTSET";
        isGITFile = true;
    }

    public Ref getRef() {
        return ref;
    }

    public void setIsGITFile(boolean b) {

        System.out.println("setIsGITFile(" + b + "): " + path);

        isGITFile = b;

        try {
            hashKey = getHash(path, "SHA-1");
        } catch (Exception ex) {
            Logger.getLogger(LTBFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean getIsGITFIle() {
        return isGITFile;
    }

    public void setOrigGitPath(String ogp) {
        origGitPath = ogp;
    }

    public String getOrigGitPath() {
        return origGitPath;
    }

    public ObjectLoader getFileLoader() {
        return fileLoader;
    }

    public void setFileLoader(ObjectLoader fl) throws Exception {
        fileLoader = fl;

        hashKey = getHash(fl, "SHA-1");
    }

    public String getFileName() {
        int x = path.lastIndexOf("/") + 1;

        return path.substring(x);
    }

    public String getFileDirectory() {
        if (isGITFile) {
            return path.substring(0, path.lastIndexOf("/"));
        } else {
            return origGitPath.substring(0, origGitPath.lastIndexOf("/"));
        }
    }

    public String getFilePathFromKey() {
        if (isGITFile) {
            return path.substring(path.indexOf("/"));
        } else {
            return origGitPath.substring(origGitPath.indexOf("/"));
        }
    }

    public String getEnvSourceMapKey() {
        if (isGITFile) {
            return path.substring(0, path.indexOf("/"));
        } else {
            return origGitPath.substring(0, origGitPath.indexOf("/"));
        }
    }

    public String getModType() {
        return modType;
    }

    public String getFullPath() {
        return path;
    }

    public String getHashKey() {
        return hashKey;
    }

    /**
     * getHash
     *
     * Calculate a hash based on the Git Repository
     *
     * @param loader
     * @param hash
     * @return
     * @throws Exception
     */
    public static String getHash(ObjectLoader loader, String hash) throws Exception {
        MessageDigest md = MessageDigest.getInstance(hash);
        DigestInputStream dis = new DigestInputStream(loader.openStream(), md);
        while (dis.available() > 0) {
            dis.read();
        }

        return Base64.encodeBytes(md.digest());
    }

    /**
     * getHash
     *
     * Calculate a hash based on a physical file
     *
     * @param file
     * @param hash
     * @return
     * @throws Exception
     */
    public static String getHash(String file, String hash) throws Exception {
        File inputFile = new File(file);

        try (FileInputStream in = new FileInputStream(inputFile)) {
            MessageDigest digester = MessageDigest.getInstance(hash);
            byte[] block = new byte[4096];
            int length;
            while ((length = in.read(block)) > 0) {
                digester.update(block, 0, length);
            }

            return Base64.encodeBytes(digester.digest());
        }
    }    
}
