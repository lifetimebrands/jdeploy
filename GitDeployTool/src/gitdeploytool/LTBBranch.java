/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitdeploytool;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import utilities.DaUtilities;

/**
 *
 * @author eliotm
 */
public class LTBBranch {

    private final Ref branchRef;
    private String jiraKey;
    private final boolean isRemoteBranch;
    private Iterable<RevCommit> branchCommits;
    private int codeEnvironmentId;
    private CodeEnvironment codeEnvironment;
    private boolean isInDB;
    private boolean isRolledIn;
    private boolean isRolledBack;
    private boolean isMasterBehind;
    private int masterBehindCount;
    private int masterAheadCount;

    public LTBBranch(Ref ref) {
        DaUtilities.writeMessage(MessageFormat.format("LTBBranch.Constructor({0})", ref != null ? ref.getName() : "NULL"), false);
        this.branchRef = ref;

        isRemoteBranch = false;
        isInDB = false;
        isRolledIn = false;
        isRolledBack = false;
    }

    public LTBBranch(Ref ref, int i, boolean b, String jiraKey) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("LTBBranch.Constructor({0}, {1}, {2})", ref != null ? ref.getName() : "NULL", b, jiraKey), false);

        this.branchRef = ref;
        this.codeEnvironmentId = 1;
        this.codeEnvironment = new CodeEnvironment(i);
        this.isRemoteBranch = b;
        this.jiraKey = jiraKey;
        isMasterBehind = checkIsMasterBehind();

        if (1 == 1) {
            branchCommits = (ref != null && !isRemoteBranch) ? this.codeEnvironment.getBranchCommits(ref.getName()) : null;
            int inDB = codeEnvironment.getJIRADBCount(jiraKey);
            int rolledIn = codeEnvironment.getJIRARolledInCount(jiraKey);

            if (inDB > 0) {
                isInDB = true;

                if (rolledIn > 0) {
                    isRolledIn = true;
                    isRolledBack = false;
                } else {
                    isRolledBack = true;
                    isRolledIn = false;
                }
            } else {
                isInDB = false;
                isRolledBack = false;
                isRolledIn = false;
            }
        } else {
            isInDB = false;
            isRolledIn = false;
            isRolledBack = false;
        }
    }

    public boolean checkoutBranch() {
        DaUtilities.writeMessage("LTBBranch.checkoutBranch()", false);

        if (!isRemoteBranch) {
            return false;
        }

        return codeEnvironment.checkoutBranch(branchRef.getName().substring(11));
    }

    public PullResult pullMaster() throws Exception {
        DaUtilities.writeMessage("LTBBranch.pullMaster()", false);

        if (!isMasterBehind) {
            throw new Exception("Master is not behind, no pull required!");
        } else {
            return codeEnvironment.pullMaster();
        }
    }

    public final boolean checkIsMasterBehind() throws IOException {

        HashMap<String, Integer> h = codeEnvironment.getAheadBehindCounts("refs/heads/master");
        isMasterBehind = (h.get("BEHIND") > 0);

        masterAheadCount = h.get("AHEAD");
        masterBehindCount = h.get("BEHIND");

        return isMasterBehind;
    }

    public boolean isIsInDB() {
        DaUtilities.writeMessage("LTBBranch.isIsInDB()", false);

        return isInDB;
    }

    public int getMasterBehindCount() {
        return masterBehindCount;
    }

    public int getMasterAheadCount() {
        return masterAheadCount;
    }

    public boolean isMasterBehind() {
        return isMasterBehind;
    }

    public boolean isIsRolledIn() {
        DaUtilities.writeMessage("LTBBranch.isIsRolledIn()", false);

        return isRolledIn;
    }

    public boolean isIsRolledBack() {
        DaUtilities.writeMessage("LTBBranch.isIsRolledBack()", false);

        return isRolledBack;
    }

    public String getEnvDescription() {
        DaUtilities.writeMessage("LTBBranch.getEnvDescription()", false);

        if (codeEnvironment != null) {
            return codeEnvironment.getCodeEnvironmentDescription();
        } else {
            return "No Environment";
        }
    }

    public int getCodeEnvironmentId() {
        DaUtilities.writeMessage("LTBBranch.getCodeEnvironmentId()", false);

        return codeEnvironmentId;
    }

    public CodeEnvironment getCodeEnvironment() {
        DaUtilities.writeMessage("LTBBranch.getCodeEnvironment()", false);

        return codeEnvironment;
    }

    public void setCodeEnvironmentId(int i) {
        DaUtilities.writeMessage(MessageFormat.format("LTBBranch.setCodeEnvironmentId({0})", i), false);

        try {
            codeEnvironmentId = i;
            codeEnvironment = new CodeEnvironment(i);
        } catch (Exception ex) {
            Logger.getLogger(LTBBranch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setCodeEnvironment(CodeEnvironment codeEnvironment) {
        DaUtilities.writeMessage("LTBBranch.setCodeEnvironment(ce)", false);
        this.codeEnvironment = codeEnvironment;
    }

    public String getBranchName() {
        DaUtilities.writeMessage("LTBBranch.getBranchName()", false);

        return branchRef.getName();
    }

    public Ref getBranchRef() {
        DaUtilities.writeMessage("LTBBranch.getBranchRef()", false);

        return branchRef;
    }

    public String getJiraKey() {
        DaUtilities.writeMessage("LTBBranch.getJiraKey()", false);

        return jiraKey;
    }

    public void setJiraKey(String jiraKey) {
        DaUtilities.writeMessage(MessageFormat.format("LTBBranch.setJiraKey({0})", jiraKey), false);

        this.jiraKey = jiraKey;
    }

    public Iterable<RevCommit> getBranchCommits() {
        DaUtilities.writeMessage("LTBBranch.getBranchCommits()", false);

        return branchCommits;
    }

    public void setBranchCommits(List<RevCommit> branchCommits) {
        DaUtilities.writeMessage("LTBBranch.setBranchCommits(bc)", false);

        this.branchCommits = branchCommits;
    }

    public boolean getIsRemoteBranch() {
        DaUtilities.writeMessage("LTBBranch.getIsRemoteBranch()", false);

        return isRemoteBranch;
    }

    public String getLocalorRemoteTag() {
        DaUtilities.writeMessage("LTBBranch.getLocalorRemoteTag()", false);

        if (isRemoteBranch) {
            return "REMOTE";
        } else {
            return "LOCAL";
        }
    }

    public HashMap<Boolean, List<String>> rollinBranch(String usrID) throws Exception {
        DaUtilities.writeMessage("LTBBranch.rollinBranch()", false);

        isRolledBack = false;
        isRolledIn = true;
        isInDB = true;

        return codeEnvironment.rollinReadyBranch(this, usrID);
    }    
    
    public HashMap<Boolean, List<String>> rollbackBranch(String usrID) throws Exception {
        DaUtilities.writeMessage("LTBBranch.rollbackBranch()", false);

        isRolledBack = true;
        isRolledIn = false;
        isInDB = true;        
                       
        return codeEnvironment.rollbackReadyBranch(this, usrID);
    }    

    public List<LTBFile> getBranchFileChanges() throws Exception {
        DaUtilities.writeMessage("LTBBranch.getBranchFileChanges()", false);

        List<LTBFile> ltbFiles = new ArrayList<>();

        if (!isRemoteBranch) {
            HashMap<String, String> h = new HashMap<>();
            for (RevCommit rc : branchCommits) {

                List<DiffEntry> diffs = codeEnvironment.getCommitDiffs(rc.getParent(0),
                        rc);
                diffs.stream().forEach((diff) -> {

                    if (!h.containsKey(diff.getNewPath())) {
                        DaUtilities.writeMessage(MessageFormat.format("---- LTBBranch.getBranchFileChanges: new file( {0} )", diff.getNewPath()), false);
                        h.put(diff.getNewPath(), diff.getChangeType().name());
                    } else if (diff.getChangeType().name().equals("ADD") && h.get(diff.getNewPath()).equals("MODIFY")) {
                        DaUtilities.writeMessage(MessageFormat.format("---- LTBBranch.getBranchFileChanges: Dupe but this is new file( {0} )", diff.getNewPath()), false);
                        h.remove(diff.getNewPath());
                        h.put(diff.getNewPath(), diff.getChangeType().name());
                    } else {
                        DaUtilities.writeMessage(MessageFormat.format("---- LTBBranch.getBranchFileChanges: Dupe, no entry made( {0} )", diff.getNewPath()), false);
                    }
                });
            }

            h.keySet().stream().forEach((k) -> {
                ltbFiles.add(new LTBFile(branchRef, h.get(k), k));
            });
        }

        return ltbFiles;
    }
}
