package utilities;
public enum QueriesEnum  {
	GET_POLICY_INFO(
			"GET_POLICY_INFO",
			"select * from poldat where polcod = 'USR-JIRA-DEPLOY'"),		
	INSERT_JIRA_LOG_ENTRY(
			"INSERT_JIRA_LOG_ENTRY",
			"insert into usr_jira_log_entry(TRAN_ID, JIRAID, BRANCHID, OBJECTTYPE, " +
                            "OBJECTNAME, SOURCEDIR, CHANGETYPE, DESTINATIONDIR, FILEHASHBITBUCKET, " +
                            "FILEHASHSTAGINGDIR, FILEHASHDESTINATIONDIR, USERID, OPERATIONTYPE, " +
                            "FILEHASHORIGINAL, FILEHASHBACKUP, BACKUPDIR, GITPATH, TRNDTE, ISROLLEDBACK) " +
			"values( JIRALOGENTRYID.nextval, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, sysdate, 0)"),
        ROLLBACK_JIRA_LOG_ENTRY("ROLLBACK_JIRA_LOG_ENTRY", 
                "update usr_jira_log_entry set isrolledback = 1 where tran_id = ?"),
        GET_BRANCH_ROLLED_IN_COUNT("GET_JIRA_ROLLED_IN_COUNT", 
                "select count(*) cnt from usr_jira_log_entry where branchid = ? and operationtype = 'DEPLOY' and nvl(isrolledback, 0) = 0"),
        GET_JIRA_ROLLED_IN_COUNT("GET_JIRA_ROLLED_IN_COUNT", 
                "select count(*) cnt from usr_jira_log_entry where jiraid = ? and operationtype = 'DEPLOY' and nvl(isrolledback, 0) = 0"),
        GET_JIRA_DB_COUNT("GET_JIRA_ROLLED_IN_COUNT", 
                "select count(*) cnt from usr_jira_log_entry where jiraid = ?"),
        GET_ROLLBACK_INFO("GET_ROLLBACK_INFO", 
                "select tran_id, changetype, destinationdir, backupdir, gitpath, objectname from usr_jira_log_entry "
                        + "where branchid = ? and operationtype = 'DEPLOY' and nvl(isrolledback, 0) = 0"),
        USR_LOGIN("USR_LOGIN", "select usr.usr_id, usr.usr_sts from les_usr_ath usr where usr.usr_id = ? and usr.usr_pswd = ? and (exists(select 1 from les_opt_ath ath\n" +
            " where ath.opt_nam = 'mnuitmgitdply' and ath.ath_typ = 'U' and ath.ath_id = usr.usr_id) or exists(select 1 from les_opt_ath ath where ath.opt_nam = 'mnuitmgitdply'\n" +
            " and ath.ath_typ = 'R' and ath.ath_id in (select role_id from les_usr_role where usr_id = usr.usr_id)))");
	
	private final String tag;
	private final String query;

	QueriesEnum(String tag, String query) {
		
		this.tag = tag;
		this.query = query;
	}

	public String getTag() {		
		return tag;
	}

	public String getQuery() {
		return query;
	}
}
