package gitdeploytool;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import utilities.DaUtilities;
import utilities.JIRALogEntry;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;

/**
 * Code Environment
 *
 * Handles mapping a repository to a coding environment
 *
 * @author eliotm
 */
public class CodeEnvironment {

    public static final int ROBBDCS = 0;
    public static final int FONTDCS = 1;
    public static final int TSTROBB = 2;

    public static final Map<Integer, String> ENV_REPO_MAP;    

    static {

        ENV_REPO_MAP = new HashMap<>();

        try {
            Properties mainProps = DaUtilities.getProperties("gitdeploytool/GitDeployToolProperties");
            if (mainProps.getProperty("ROBREPOSITORY").equals("ON")) {
                String dcsDir = mainProps.getProperty("ROBBDCS");
                ENV_REPO_MAP.put(ROBBDCS, dcsDir);
            }

            if (mainProps.getProperty("FONREPOSITORY").equals("ON")) {
                String dcsDir = mainProps.getProperty("FONTDCS");
                ENV_REPO_MAP.put(FONTDCS, dcsDir);                
            }

            if (mainProps.getProperty("TSTREPOSITORY").equals("ON")) {
                String dcsDir = mainProps.getProperty("TESTDCS");
                ENV_REPO_MAP.put(TSTROBB, dcsDir);        
            }
        } catch (IOException ex) {
            Logger.getLogger(CodeEnvironment.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static final Map<Integer, String> ENV_REPO_DESC_MAP;

    static {
        ENV_REPO_DESC_MAP = new HashMap<>();

        ENV_REPO_DESC_MAP.put(ROBBDCS, "Robbinsville DCS Repository");
        ENV_REPO_DESC_MAP.put(FONTDCS, "Fontana DCS Repository");
        ENV_REPO_DESC_MAP.put(TSTROBB, "Test Repository");
    }

    private static final String STAGING_DIR = "ROLLINDIR";
    private final String ENV_DB_CONN;

    private final int environment;
    private final String repoLocation;
    private final LTBRepository ltbRepository;

    private final Map<String, String> repoDirToEnvDirMap = new HashMap<>();

    /**
     * Code Environment
     *
     * Constructor
     *
     * @param env
     * @throws Exception
     */
    public CodeEnvironment(int env) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.Constructor({0})", ENV_REPO_DESC_MAP.get(env)), false);
        environment = env;

        initializeMaps();
        repoLocation = ENV_REPO_MAP.get(env);
        ltbRepository = new LTBRepository(repoLocation);

        Properties props = DaUtilities.getProperties("gitdeploytool/DBConnectProperties");
        Properties mainProps = DaUtilities.getProperties("gitdeploytool/GitDeployToolProperties");
        String runMode = mainProps.getProperty("MODE");

        if (env == ROBBDCS && !mainProps.getProperty("ROBREPOSITORY").equals("ON")) {
            throw new Exception("Environment not configured (ROBBDCS)");
        }
        if (env == FONTDCS && !mainProps.getProperty("FONREPOSITORY").equals("ON")) {
            throw new Exception("Environment not configured (ROBBDCS)");
        }
        if (env == TSTROBB && !mainProps.getProperty("TSTREPOSITORY").equals("ON")) {
            throw new Exception("Environment not configured (ROBBDCS)");
        }

        switch (env) {
            case ROBBDCS:
                ENV_DB_CONN = props.getProperty(MessageFormat.format("{0}{1}", runMode, "ROBBDCS"));
                break;
            case FONTDCS:
                ENV_DB_CONN = props.getProperty(MessageFormat.format("{0}{1}", runMode, "FONTDCS"));
                break;
            case TSTROBB:
                ENV_DB_CONN = props.getProperty(MessageFormat.format("{0}{1}", runMode, "TSTROBB"));
                break;
            default:
                ENV_DB_CONN = "NO-ENV-CONN";
        }

        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.Constructor: DBCONN = {0}", ENV_DB_CONN), false);
    }

    public HashMap<String, Integer> getAheadBehindCounts(String branch) throws IOException {
        return ltbRepository.getAheadBehindCounts(branch);
    }

    public String getCodeEnvironmentDescription() {
        return CodeEnvironment.ENV_REPO_DESC_MAP.get(environment);
    }

    public Iterable<RevCommit> getBranchCommits(String branchId) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.getBranchCommits({0})", branchId), false);
        return ltbRepository.rangeWalk(branchId);
    }

    /**
     * initializeMaps
     *
     * This will initialize the maps used for the requested environment
     *
     * @throws Exception
     */
    private void initializeMaps() throws Exception {

        DaUtilities.writeMessage("CodeEnvironment.initializeMaps()", false);
        Properties mainProps = DaUtilities.getProperties("gitdeploytool/GitDeployToolProperties");
        String runMode = mainProps.getProperty("MODE");

        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initalizeMaps: ENV - ({0})", ENV_REPO_DESC_MAP.get(environment)), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initalizeMaps: Mode - ({0})", runMode), false);

        // Setup the Environment Directory Mappings
        if (environment == TSTROBB) {                                  
            Properties props = DaUtilities.getProperties("gitdeploytool/TstRobbProperties");
            repoDirToEnvDirMap.put(STAGING_DIR, props.getProperty(MessageFormat.format("{0}{1}", runMode, "STAGING_DIR")));
            repoDirToEnvDirMap.put("src", props.getProperty(MessageFormat.format("{0}{1}", runMode, "SRC")));
            repoDirToEnvDirMap.put("scripts", props.getProperty(MessageFormat.format("{0}{1}", runMode, "SCRIPTS")));
            repoDirToEnvDirMap.put("reports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "REPORTS")));
            repoDirToEnvDirMap.put("oraclereports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "ORACLEREPORTS")));
            repoDirToEnvDirMap.put("labels", props.getProperty(MessageFormat.format("{0}{1}", runMode, "LABELS")));
            repoDirToEnvDirMap.put("data", props.getProperty(MessageFormat.format("{0}{1}", runMode, "DATA")));
            repoDirToEnvDirMap.put("dda_exports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "DDA_EXPORTS")));
        } else if (environment == ROBBDCS) {
            Properties props = DaUtilities.getProperties("gitdeploytool/RobDCSProperties");
            repoDirToEnvDirMap.put(STAGING_DIR, props.getProperty(MessageFormat.format("{0}{1}", runMode, "STAGING_DIR")));
            repoDirToEnvDirMap.put("src", props.getProperty(MessageFormat.format("{0}{1}", runMode, "SRC")));
            repoDirToEnvDirMap.put("scripts", props.getProperty(MessageFormat.format("{0}{1}", runMode, "SCRIPTS")));
            repoDirToEnvDirMap.put("reports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "REPORTS")));
            repoDirToEnvDirMap.put("oraclereports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "ORACLEREPORTS")));
            repoDirToEnvDirMap.put("labels", props.getProperty(MessageFormat.format("{0}{1}", runMode, "LABELS")));
            repoDirToEnvDirMap.put("data", props.getProperty(MessageFormat.format("{0}{1}", runMode, "DATA")));
            repoDirToEnvDirMap.put("dda_exports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "DDA_EXPORTS")));
        } else if (environment == FONTDCS) {
            Properties props = DaUtilities.getProperties("gitdeploytool/FontDCSProperties");
            repoDirToEnvDirMap.put(STAGING_DIR, props.getProperty(MessageFormat.format("{0}{1}", runMode, "STAGING_DIR")));
            repoDirToEnvDirMap.put("src", props.getProperty(MessageFormat.format("{0}{1}", runMode, "SRC")));
            repoDirToEnvDirMap.put("scripts", props.getProperty(MessageFormat.format("{0}{1}", runMode, "SCRIPTS")));
            repoDirToEnvDirMap.put("Reports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "REPORTS")));
            repoDirToEnvDirMap.put("oraclereports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "ORACLEREPORTS")));
            repoDirToEnvDirMap.put("labels", props.getProperty(MessageFormat.format("{0}{1}", runMode, "LABELS")));
            repoDirToEnvDirMap.put("data", props.getProperty(MessageFormat.format("{0}{1}", runMode, "DATA")));
            repoDirToEnvDirMap.put("dda_exports", props.getProperty(MessageFormat.format("{0}{1}", runMode, "DDA_EXPORTS")));
        } else {
            throw new Exception("Unsupported Environment, repository to "
                    + "environment mappings are not set up!");
        }

        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: {0} - {1})", STAGING_DIR, repoDirToEnvDirMap.get(STAGING_DIR)), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: src - {0}", repoDirToEnvDirMap.get("src")), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: scripts - {0}", repoDirToEnvDirMap.get("scripts")), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: Reports - {0}", repoDirToEnvDirMap.get("Reports")), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: reports - {0}", repoDirToEnvDirMap.get("reports")), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: oraclereports - {0}", repoDirToEnvDirMap.get("oraclereports")), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: labels - {0}", repoDirToEnvDirMap.get("labels")), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: data - {0}", repoDirToEnvDirMap.get("data")), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.initializeMaps: dda_exports - {0}", repoDirToEnvDirMap.get("dda_exports")), false);
    }

    public static List<LTBBranch> findEnvironmentsForJIRAKey(String jk) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.findEnvironmentForJIRAKey({0})", jk), false);
        ArrayList<LTBBranch> branchList = new ArrayList<>();

        for (int i : ENV_REPO_MAP.keySet()) {

            DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.findEnvironmentForJIRAKey: Checking envirionment: {0}", ENV_REPO_DESC_MAP.get(i)), false);
            LTBRepository r = new LTBRepository(ENV_REPO_MAP.get(i));

            boolean foundLocal = false;
            List<Ref> branches = r.getBranches();
            for (Ref ref : branches) {
                if (ref.getName().contains(jk + " ") || ref.getName().contains(jk + "-")) {
                    DaUtilities.writeMessage("---- CodeEnvironment.findEnvironmentsForJIRAKey: Found it Local", false);
                    branchList.add(new LTBBranch(ref, i, false, jk));
                    foundLocal = true;
                }
            }

            if (!foundLocal) {
                Collection<Ref> remoteBranches = r.getRemoteBranches();
                for (Ref ref : remoteBranches) {

                    if (ref.getName().contains(jk + " ") || ref.getName().contains(jk + "-")) {
                        DaUtilities.writeMessage("---- CodeEnvironment.findEnvironmentsForJIRAKey: Found it Remote", false);
                        branchList.add(new LTBBranch(ref, i, true, jk));
                    }
                }
            }
        }

        return branchList;
    }

    public static LTBBranch findEvironmentForJIRAKey(String jk, String env) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.findEnvironmentForJIRAKey({0}, {1})", jk, env), false);

        for (int i : ENV_REPO_MAP.keySet()) {

            if (ENV_REPO_DESC_MAP.get(i).equals(env)) {
                DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.findEnvironmentForJIRAKey: Checking envirionment: {0}", ENV_REPO_DESC_MAP.get(i)), false);
                LTBRepository r = new LTBRepository(ENV_REPO_MAP.get(i));

                List<Ref> branches = r.getBranches();
                for (Ref ref : branches) {
                    if (ref.getName().contains(jk + " ") || ref.getName().contains(jk + "-")) {
                        DaUtilities.writeMessage("---- CodeEnvironment.findEnvironmentForJIRAKey: Found it", false);
                        return new LTBBranch(ref, i, false, jk);
                    }
                }

                DaUtilities.writeMessage("---- CodeEnvironment.findEnvironmentForJIRAKey: Not Found", false);
            }
        }

        DaUtilities.writeMessage("---- CodeEnvironment.findEnvironmentForJIRAKey: Not Found Local", false);

        return null;
    }

    public static LTBBranch findRemoteHeadForJIRAKey(String jk, String env) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.findRemoteHeadForJIRAKey({0})", jk), false);
        for (int i : ENV_REPO_MAP.keySet()) {

            if (ENV_REPO_DESC_MAP.get(i).equals(env)) {

                DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.findRemoteHeadForJIRAKey: Checking envirionment: {0}", env), false);

                LTBRepository r = new LTBRepository(ENV_REPO_MAP.get(i));
                Collection<Ref> branches = r.getRemoteBranches();
                for (Ref ref : branches) {

                    if (ref.getName().contains(jk + " ") || ref.getName().contains(jk + "-")) {
                        DaUtilities.writeMessage("---- CodeEnvironment.findRemoteHeadForJIRAKey: Found it", false);
                        return new LTBBranch(ref, i, true, jk);
                    }
                }

                DaUtilities.writeMessage("---- CodeEnvironment.findRemoteHeadForJIRAKey: Not Found", false);
            }
        }

        DaUtilities.writeMessage("---- CodeEnvironment.findRemoteHeadForJIRAKey: Not Found Remote", false);

        return null;
    }

    public boolean checkoutBranch(String branchName) {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.checkoutBranch({0})", branchName), false);
        return ltbRepository.checkoutBranch(branchName);
    }

    public boolean isDeployed(String branchId) throws ClassNotFoundException, SQLException {

        try (Connection dbConn = DaUtilities.getOracleConnection(ENV_DB_CONN)) {
            UtilPreparedStatement ups = new UtilPreparedStatement(
                    QueriesEnum.GET_BRANCH_ROLLED_IN_COUNT.getQuery());

            ups.setString(1, branchId);

            int cnt;
            try (ResultSet rs = DaUtilities.runQuery(dbConn, ups, true)) {
                rs.next();
                cnt = rs.getInt("cnt");
            }

            System.out.println("CNT: " + cnt);
            return (cnt > 0);
        }
    }

    public HashMap<Boolean, List<String>> rollbackReadyBranch(LTBBranch ltb, String usrID) throws Exception {
        DaUtilities.writeMessage("CodeEnvironment.rollbackReadyBranch(ltb)", false);

        HashMap<Boolean, List<String>> returnMap = new HashMap<>();
        boolean aOK = true;
        List<String> ls = new ArrayList<>();

        try (Connection dbConn = DaUtilities.getOracleConnection(ENV_DB_CONN)) {
            UtilPreparedStatement ups = new UtilPreparedStatement(
                    QueriesEnum.GET_ROLLBACK_INFO.getQuery());

            ups.setString(1, ltb.getBranchName());

            JIRALogEntry jle = new JIRALogEntry(ltb.getJiraKey(), "ROLLBACK", "", usrID);
            int fileCounter = 0;
            try (ResultSet rs = DaUtilities.runQuery(dbConn, ups, true)) {
                while (rs.next()) {
                    fileCounter++;

                    String fOrig = rs.getString("backupdir");
                    String gitPath = rs.getString("gitpath");
                    String sourceDir = rs.getString("destinationdir");
                    String cType = rs.getString("changetype");
                    String oName = rs.getString("objectname");
                    long tranId = rs.getLong("tran_id");

                    jle.setBranchId(ltb.getBranchName());

                    Ref ref = ltb.getBranchRef();
                    LTBFile f;

                    //setting action to MODIFY for all so all is backed up
                    if (cType.equals("ADD")) {

                        System.out.println("Add block: " + sourceDir);
                        f = new LTBFile(ref, cType, sourceDir);
                    } else {
                        System.out.println("Other block: " + fOrig);
                        f = new LTBFile(ref, cType, fOrig);
                    }

                    f.setIsGITFile(false);
                    f.setOrigGitPath(gitPath);

                    jle.setObjectType("FILE");
                    jle.setObjectName(f.getFileName());
                    jle.setChangeType(f.getModType());                    

                    jle.createLogEntry(MessageFormat.format("{0} - {1}",
                            jle.getObjectName(), f.getHashKey()));

                    if (cType.equals("ADD")) {
                        aOK = backupFile(f, jle);
                        jle.setBackupResult(aOK);
                        jle.setRollinResult(aOK);
                        jle.setStageResult(aOK);
                    } else {
                        stageAndBackupFile(f, true, jle);
                    }

                    jle.writeEventLogOutput(getStagingDirectory() + "/" + jle.getProcId() + "/");
                    ls = jle.getEventLog();

                    if (!jle.getBackupResult() || !jle.getRollinResult()
                            || !jle.getStageResult()) {
                        aOK = false;
                    }

                    jle.insert(dbConn);

                    UtilPreparedStatement upsx = new UtilPreparedStatement(
                            QueriesEnum.ROLLBACK_JIRA_LOG_ENTRY.getQuery());

                    upsx.setLong(1, tranId);

                    DaUtilities.executeUpdate(dbConn, upsx, true);
                }
            }

            if (fileCounter == 0) {
                DaUtilities.writeMessage("Nothing to roll in", true);
                throw new Exception("Nothing to Roll Back, Project may not be rolled in.");
            }
        }

        returnMap.put(aOK, ls);
        return returnMap;
    }    

    public PullResult pullMaster() throws Exception {
        return ltbRepository.pullMaster();
    }

    public int getJIRADBCount(String jiraKey) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.getJIRADBCount({0})", jiraKey), false);
        int cnt = 0;
        try (Connection dbConn = DaUtilities.getOracleConnection(ENV_DB_CONN)) {
            UtilPreparedStatement ups = new UtilPreparedStatement(
                    QueriesEnum.GET_JIRA_DB_COUNT.getQuery());

            ups.setString(1, jiraKey);

            try (ResultSet rs = DaUtilities.runQuery(dbConn, ups, true)) {
                while (rs.next()) {
                    cnt = rs.getInt("cnt");
                }
            }
        }

        return cnt;
    }

    public int getJIRARolledInCount(String jiraKey) throws Exception {
        int cnt = 0;
        try (Connection dbConn = DaUtilities.getOracleConnection(ENV_DB_CONN)) {
            UtilPreparedStatement ups = new UtilPreparedStatement(
                    QueriesEnum.GET_JIRA_ROLLED_IN_COUNT.getQuery());

            ups.setString(1, jiraKey);

            try (ResultSet rs = DaUtilities.runQuery(dbConn, ups, true)) {
                while (rs.next()) {
                    cnt = rs.getInt("cnt");
                }
            }
        }

        return cnt;
    }

    public HashMap<Boolean, List<String>> rollinReadyBranch(LTBBranch ltb, String usrID) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.rollinReadyBranch({0})", ltb.getBranchName()), false);        
        
        HashMap<Boolean, List<String>> returnMap = new HashMap<>();
        boolean aOK = true;
        List<String> ls = new ArrayList<>();

        if (isDeployed(ltb.getBranchName())) {
            DaUtilities.writeMessage("---- CodeEnvironment.rollinReadyBranch: Branch Rolled In Already . . .", false);
            throw new Exception("This Branch is already rolled in, only a roll back is valid!");
        }

        try (Connection dbConn = DaUtilities.getOracleConnection(ENV_DB_CONN)) {
            Ref ref = ltb.getBranchRef();

            RevCommit thisCommit = getCommit(ref.getObjectId().getName());
            List<LTBFile> changedFileList = ltb.getBranchFileChanges();

            JIRALogEntry jle = new JIRALogEntry(ltb.getJiraKey(), "DEPLOY", "", usrID);
            jle.setBranchId(ltb.getBranchName());

            DaUtilities.writeMessage("---- CodeEnvironment.rollinReadyBranch: Rolling in files", false);
            for (LTBFile file : changedFileList) {

                DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.rollinReadyBranch: Rolling in {0}", file.getFileName()), false);
                jle.setObjectType("FILE");
                jle.setObjectName(file.getFileName());                               
                
                jle.setChangeType(file.getModType());                
                jle.setGitPath(file.getFullPath());

                file.setFileLoader(getFileLoader(thisCommit, file.getFullPath()));
                jle.setFileHashBitBucket(file.getHashKey());

                jle.createLogEntry(MessageFormat.format("{0} - {1}",
                        jle.getObjectName(), jle.getFileHashBitBucket()));

                DaUtilities.writeMessage("---- CodeEnvironment.rollinReadyBranch: Starting stageAndBackup . . .", false);
                stageAndBackupFile(file, true, jle);
                DaUtilities.writeMessage("---- CodeEnvironment.rollinReadyBranch: Complete . . .", false);

                DaUtilities.writeMessage("---- CodeEnvironment.rollinReadyBranch: Logging Output", false);
                jle.writeEventLogOutput(getStagingDirectory() + "/" + jle.getProcId() + "/");
                ls = jle.getEventLog();

                DaUtilities.writeMessage("---- CodeEnvironment.rollinReadyBranch: Checking Results", false);
                if (!jle.getBackupResult() || !jle.getRollinResult()
                        || !jle.getStageResult()) {
                    aOK = false;
                }

                DaUtilities.writeMessage("---- CodeEnvironment.rollinReadyBranch: Inserting usr_jira_log_entry record", false);

                jle.insert(dbConn);
            }
        }

        returnMap.put(aOK, ls);
               
        return returnMap;
    }    

    /**
     * stageAndBackupFile
     *
     * This will take in an LTBFile and attempt to stage it to the environments
     * staging directory. The file will only be backed up if the file mode is
     * modify.
     *
     * @param f - LTBFile to Be Staged
     * @param rollinCode - Set to Also Roll In the code
     * @param jle
     * @throws Exception
     */
    public void stageAndBackupFile(LTBFile f, boolean rollinCode, JIRALogEntry jle)
            throws Exception {

        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.stageAndBackupFile({0})", f.getFileName()), false);
        DaUtilities.writeMessage("---- CodeEnvironment.stageAndBackupFile: Staging . . .", false);
        jle.setStageResult(stageFile(f, jle));
        jle.createLogEntry(MessageFormat.format("Stage Hash Values Match: {0}", jle.getStageResult()));

        if (!f.getModType().equals("ADD")) {
            DaUtilities.writeMessage("---- CodeEnvironment.stageAndBackupFile: Backing Up . . .", false);
            jle.setBackupResult(backupFile(f, jle));
            jle.createLogEntry(MessageFormat.format("Backup Hash Values Match: {0}", jle.getBackupResult()));
        } else {
            jle.setBackupResult(true);
        }

        DaUtilities.writeMessage("---- CodeEnvironment.stageAndBackupFile: Rolling In . . .", false);
        jle.setRollinResult(rollinFile(f, jle));
        DaUtilities.writeMessage("---- CodeEnvironment.stageAndBackupFile: Complete . . .", false);
        jle.createLogEntry(MessageFormat.format("Rollin Hash Values Match: {0}", jle.getRollinResult()));
    }    

    /**
     * stagingFile
     *
     * Will Roll in the file to the code environment
     *
     * @param f - LTBFile to Roll In
     * @return - true if file hashes match
     * @throws Exception
     */
    private boolean rollinFile(LTBFile f, JIRALogEntry jle) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.rollinFile({0})", f.getFileName()), false);

        String stagingDir = getStagingDirectory() + "/" + jle.getProcId() + "/"
                + jle.getJiraID() + "/" + f.getModType() + "/"
                + f.getFileDirectory();
        String stagingFile = stagingDir + "/" + f.getFileName();
        String envFileName = repoDirToEnvDirMap.get(f.getEnvSourceMapKey())
                + f.getFilePathFromKey();

        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.rollinFile: stageing file {0}", stagingFile), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.rollinFile: destination {0}", envFileName), false);        
        
        File newFile = new File(stagingFile);
        File oldFile = new File(envFileName);

        DaUtilities.writeMessage("---- CodeEnvironment.rollinFile: Copying . . .", false);
        Files.copy(newFile.toPath(), oldFile.toPath(), REPLACE_EXISTING);

        DaUtilities.writeMessage("---- CodeEnvironment.rollinFile: Getting Hashes . . .", false);
        String stageHash = LTBFile.getHash(newFile.getAbsolutePath(), "SHA-1");
        String rollinHash = LTBFile.getHash(oldFile.getAbsolutePath(), "SHA-1");

        DaUtilities.writeMessage("---- CodeEnvironment.rollinFile: Setting Up JLE Log Entries . . .", false);
        jle.setDestinationDir(envFileName);
        jle.setFileHashDestinationDir(rollinHash);

        jle.createLogEntry(MessageFormat.format("Rollin File {0}", f.getFileName()));
        jle.createLogEntry(MessageFormat.format(" ---- Source: {0}", stagingFile));
        jle.createLogEntry(MessageFormat.format(" ---- Dest  : {0}", envFileName));
        jle.createLogEntry(MessageFormat.format(" ---- {0} : {1}", rollinHash, stageHash));

        DaUtilities.writeMessage("---- CodeEnvironment.rollinFile: Returning . . .", false);
        return stageHash.equals(rollinHash);
    }    

    /**
     * backupFile
     *
     * This will backup the environments source file that will be replaced with
     * the version from the repository / commit
     *
     * @return - hash of the file (SHA-1)
     */
    private boolean backupFile(LTBFile f, JIRALogEntry jle) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.backupFile({0})", f.getFileName()), false);

        String backupFileDir = getStagingDirectory() + "/" + jle.getProcId()
                + "/" + jle.getJiraID() + "/BACKUP/" + f.getFileDirectory();
        String backupFileName = getStagingDirectory() + "/" + jle.getProcId()
                + "/" + jle.getJiraID() + "/BACKUP/" + f.getFileDirectory()
                + "/" + f.getFileName();
        String envFileName = repoDirToEnvDirMap.get(f.getEnvSourceMapKey())
                + f.getFilePathFromKey();        
        
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.backupFile: f.getEnvSourceMapkey {0}", f.getEnvSourceMapKey()), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.backupFile: f.getEnvSourceMapkey {0}", f.getFilePathFromKey()), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.backupFile: batckup {0}", envFileName), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.backupFile: batckup {0}", envFileName), false);
        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.backupFile: destination {0}", backupFileDir), false);

        File backupDir = new File(backupFileDir);
        if (!backupDir.exists()) {
            backupDir.mkdirs();
        }

        File sourceFile = new File(envFileName);
        File backupFile = new File(backupFileName);

        String sourceHash = LTBFile.getHash(sourceFile.getAbsolutePath(), "SHA-1");

        if (f.getIsGITFIle()) {
            System.out.println("Copy . . .");
            Files.copy(sourceFile.toPath(), backupFile.toPath(), REPLACE_EXISTING);
        } else {
            System.out.println("Move . . .");
            Files.move(sourceFile.toPath(), backupFile.toPath(), REPLACE_EXISTING);
        }

        String backupHash = LTBFile.getHash(backupFile.getAbsolutePath(), "SHA-1");

        jle.setBackupDir(backupFileName);
        jle.setFileHashOriginalFile(sourceHash);
        jle.setFileHashBackupFile(backupHash);

        System.out.println(sourceHash + " : " + backupHash);
        jle.createLogEntry(MessageFormat.format("Backup File {0}", f.getFileName()));
        jle.createLogEntry(MessageFormat.format(" ---- Source: {0}", sourceFile));
        jle.createLogEntry(MessageFormat.format(" ---- Dest  : {0}", backupFileName));
        jle.createLogEntry(MessageFormat.format(" ---- {0} : {1}", sourceHash, backupHash));

        return sourceHash.equals(backupHash);
    }    

    /**
     * stageFile
     *
     * This will take in an LTBFile and attempt to stage it to the environments
     * staging directory
     *
     * @return - hash of file created from the repository
     * @throws Exception
     */
    private boolean stageFile(LTBFile f, JIRALogEntry jle) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("CodeEnvironment.stageFile({0})", f.getFileName()), false);

        String stagingDir = getStagingDirectory() + "/" + jle.getProcId() + "/"
                + jle.getJiraID() + "/" + f.getModType() + "/"
                + f.getFileDirectory();
        String stagingFile = stagingDir + "/" + f.getFileName();

        DaUtilities.writeMessage(MessageFormat.format("---- CodeEnvironment.stageFile: stage to {0}", stagingDir), false);

        File stagingDirectory = new File(stagingDir);
        if (!stagingDirectory.exists()) {
            stagingDirectory.mkdirs();
        }
        
        if (f.getIsGITFIle()) {
            OutputStream fOut = new FileOutputStream(stagingFile);

            f.getFileLoader().copyTo(fOut);
        } else {            
            File sourceFile = new File(f.getFullPath());
            File backupFile = new File(stagingFile);
            
            Files.copy(sourceFile.toPath(), backupFile.toPath(), REPLACE_EXISTING);
        }

        String stageHash = LTBFile.getHash(stagingFile, "SHA-1");

        jle.setSourceDir(stagingFile);
        jle.setFileHashStagingDir(stageHash);
        
        jle.createLogEntry(MessageFormat.format("Staging File {0}", f.getFileName()));
        jle.createLogEntry(MessageFormat.format(" ---- Source: {0}", f.getFullPath()));
        jle.createLogEntry(MessageFormat.format(" ---- Dest  : {0}", stagingFile));
        jle.createLogEntry(MessageFormat.format(" ---- {0} : {1}", f.getHashKey(), stageHash));

        return stageHash.equals(f.getHashKey());
    }    

    /**
     * getStagingDirectory
     *
     * Get the Staging Directory from the maps
     *
     * @return
     */
    public String getStagingDirectory() {
        return repoDirToEnvDirMap.get(STAGING_DIR);
    }

    /**
     * getRepositoryLocation
     *
     * Get the Repository Path
     *
     * @return
     */
    public String getRepositoryLocation() {
        return ENV_REPO_MAP.get(environment);
    }

    /**
     * getBranches
     *
     * Get the Remote Repository Branches
     *
     * @return - List of Branch References
     * @throws GitAPIException
     */
    public List<Ref> getBranches() throws GitAPIException {
        return ltbRepository.getBranches();
    }

    public Ref getBranchRef(String s) throws Exception {
        return ltbRepository.getBranchRef(s);
    }

    /**
     * getAllCommits
     *
     * Get All Commits for this Environments Repository
     *
     * @return - Iterable Commit Object
     * @throws IOException
     * @throws GitAPIException
     */
    public Iterable<RevCommit> getAllCommits() throws IOException, GitAPIException {
        return ltbRepository.getAllCommits();
    }

    /**
     * getMasterCommit
     *
     * Return the Commit that represents the master head
     *
     * @return
     */
    public RevCommit getMasterCommit() {
        return ltbRepository.getMasterCommit();
    }

    /**
     * getCommit
     *
     * Get a Commit based on the 20 Digit SHA-1
     *
     * @param cId - Commit ID (SHA-1)
     * @return - RevCommit Object for that ID
     * @throws IOException
     */
    public RevCommit getCommit(String cId) throws IOException {
        return ltbRepository.getCommit(cId);
    }

    /**
     * getCommitDiffs
     *
     * Get a List of the differences between two commit objects.
     *
     * @param c1 - Commit Object 1
     * @param c2 - Commit Object 2
     * @return - List of Differences
     * @throws IOException
     */
    public List<DiffEntry> getCommitDiffs(RevCommit c1, RevCommit c2) throws IOException {
        return ltbRepository.getCommitDiffs(c1, c2);
    }

    /**
     * isMergedToMaster
     *
     * Return true / false if this commit has been merged to master
     *
     * @param rc
     * @return
     * @throws Exception
     */
    public boolean isMergedToMaster(RevCommit rc) throws Exception {
        return ltbRepository.isMergedToMaster(rc);
    }

    /**
     * getFileLoader
     *
     * @param commit - The commit to locate the file version in
     * @param fId - The full path file name
     * @return - The ObjectLoader for the file
     * @throws Exception
     */
    public ObjectLoader getFileLoader(RevCommit commit, String fId) throws Exception {
        return ltbRepository.getFileLoader(commit, fId);
    }
}
