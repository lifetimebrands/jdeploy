package utilities;

public enum DatabaseURLEnum {
    PROD    ("jdbc:oracle:thin:@//172.16.1.129:1521/PROD", "PROD", "PROD"),
    NEWPROD ("jdbc:oracle:thin:@//robtestdb:1521/NEWPROD", "NEWPROD", "NEWPROD" ),
    FONTEST ("jdbc:oracle:thin:@//192.168.12.48:1521/FONTEST.HOAN.COM", "fontest", "fontest"),
    FONPROD ("jdbc:oracle:thin:@//192.168.12.60:1521/FONPROD", "FONPROD", "FONPROD"),
    DEV2013 ("jdbc:oracle:thin:@//robdevfdb.robbinsville.com:1521/ROBDEVF", "devf", "devf"),
    JDAPROD ("jdbc:oracle:thin:@//njrob-proddb.robbinsville.com:1521/ROBPROD.ROBBINSVILLE.COM", "robprod", "robprod");
    
    private String url;
    private String un;
    private String pw;
    
    DatabaseURLEnum( String url, String un, String pw ) {
    	this.url = url;
    	this.un  = un;
    	this.pw  = pw;
    }
    
    public String getUrl() {
    	return url;
    }
    
    public String getUn() {
    	return un;
    }
    
    public String getPw() {
    	return pw;
    }
}
