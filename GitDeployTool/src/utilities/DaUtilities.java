package utilities;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

public class DaUtilities {

    public static Connection getOracleConnection(String dbString)
            throws ClassNotFoundException, SQLException {

        DaUtilities.writeMessage("DB Connection: " + dbString, false);
        DatabaseURLEnum dbInfo = null;

        if (dbString.equals("FONTEST")) {
            dbInfo = DatabaseURLEnum.FONTEST;
        }
        if (dbString.equals("FONPROD")) {
            dbInfo = DatabaseURLEnum.FONPROD;
        }
        if (dbString.equals("NEWPROD")) {
            dbInfo = DatabaseURLEnum.NEWPROD;
        }
        if (dbString.equals("PROD")) {
            dbInfo = DatabaseURLEnum.PROD;
        }
        if (dbString.equals("DEV2013")) {
            dbInfo = DatabaseURLEnum.DEV2013;
        }
        if (dbString.equals("JDAPROD")) {
            dbInfo = DatabaseURLEnum.JDAPROD;
        }

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = DriverManager.getConnection(dbInfo.getUrl(), dbInfo.getUn(),
                dbInfo.getPw());
        conn.setAutoCommit(false);

        return conn;
    }

    public static Properties getProperties(String resourceName) throws IOException {
        Properties props = null;
        ClassLoader loader = ClassLoader.getSystemClassLoader();
        InputStream is = loader.getResourceAsStream(resourceName);

        if (is != null) {
            props = new Properties();
            props.load(is);
            is.close();
            is = null;
        }

        loader = null;
        return props;
    }    

    public static void writeMessage(String msg, boolean errorMsg) {

        Timestamp ts = new Timestamp(System.currentTimeMillis());
        StringBuffer logMsg = new StringBuffer();
        if (errorMsg) {
            System.out.println(ts + ": ***** ERROR *****");
        }
        logMsg.append(ts);
        logMsg.append(": ");
        logMsg.append(msg);

        System.out.println(logMsg);
    }

    public static HashMap<String, String> Evaluate(HashMap<String, String> h1,
            HashMap<String, String> h2) {

        Iterator<String> keys = h1.keySet().iterator();
        HashMap<String, String> failMap = new HashMap<>();
        while (keys.hasNext()) {
            String key = keys.next();
            String s1 = h1.get(key);
            String s2 = h2.get(key);

            if (!DaUtilities.compare(s1, s2)) {
                failMap.put(key, s1 + " / " + s2);
            }
        }

        return failMap;
    }

    public static boolean compare(String s1, String s2) {

        if (s1 == null || s1.trim().length() == 0) {
            s1 = "NULL";
        }
        if (s2 == null || s2.trim().length() == 0) {
            s2 = "NULL";
        }
        s1 = s1.trim();
        s2 = s2.trim();

        return (s1.equals(s2));
    }

    public static ResultSet runQuery(Connection conn, UtilPreparedStatement ups, boolean logitFlag)
            throws SQLException {
        Statement st = conn.createStatement();

        if(logitFlag) {
            DaUtilities.writeMessage("---- DaUtilities.runQuery Executing SQL: " + ups.getTranslatedSql(), false);
        }
        
        ResultSet rs = st.executeQuery(ups.getTranslatedSql());

        return rs;
    }

    public static boolean executeUpdate(Connection conn,
            UtilPreparedStatement ups, boolean doCommit) throws Exception {

        Statement st = conn.createStatement();

        DaUtilities.writeMessage("Executing SQL: " + ups.getTranslatedSql(), false);
        int iRows = st.executeUpdate(ups.getTranslatedSql());
        if (iRows == 1) {
            DaUtilities.writeMessage("DaUtilities.executeUpdate: good result: " + iRows, false);
            if (doCommit) {
                conn.commit();
            }
            return true;
        } else {
            DaUtilities.writeMessage("DaUtilities.executeUpdate: fail, bad results: " + iRows, true);
            if (doCommit) {
                conn.rollback();
            }
            return false;
        }
    }

    public static Float translateTextToCurrency(String text) {
        try {
            writeMessage("DaUtilities.translateTextToCurrency: " + text, false);
            float f = Float.parseFloat(text);
            return f;
        } catch (Exception ex) {
            return (float) 0;
        }
    }

    public static Date getDateFromString(String sod) throws Exception {

        //Sample format from files: 2009-10-02 12:20:16.323000000
        sod = sod.trim();
        DaUtilities.writeMessage("DaUtilities.getDateFromString: " + sod, false);
        if (sod.length() == 0) {
            return null;
        }

        int year = Integer.parseInt(sod.substring(0, 4));
        int month = parseStringToInt(sod.substring(5, 7));
        int day = parseStringToInt(sod.substring(8, 10));
        int hour = 0;
        int min = 0;
        int sec = 0;

        if (sod.length() > 10) {
            try {
                hour = parseStringToInt(sod.substring(11, 13));
                min = parseStringToInt(sod.substring(14, 16));
                sec = parseStringToInt(sod.substring(17, 19));
            } catch (Exception ex) {
                hour = 0;
                min = 0;
                sec = 0;
            }
        }

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, min);
        c.set(Calendar.SECOND, sec);

        DaUtilities.writeMessage("DaUtilities.getDateFromString Return: " + c.getTime(), false);
        return c.getTime();
    }

    public static int parseStringToInt(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String checkForNullInNotNullableField(String s) {
        /* 
         * In the file I get no data for fields defined with constraint NOT NULL 
         * so I need to do this dumb thing
         */
        if (s == null || s.length() < 1) {
            return " ";
        } else {
            return s;
        }
    }

    public static HashMap<String, DTCPolicy> getDTCPolicyData(Connection conn)
            throws SQLException {

        HashMap<String, DTCPolicy> pMap = new HashMap<>();

        Statement s = conn.createStatement();
        ResultSet rs = s.executeQuery(QueriesEnum.GET_POLICY_INFO.getQuery());
        while (rs.next()) {

            String polcod = rs.getString("polcod");
            String polval = rs.getString("polval");
            String polvar = rs.getString("polvar");
            long srtseq = rs.getLong("srtseq");
            String rtstr1 = rs.getString("rtstr1");
            String rtstr2 = rs.getString("rtstr2");
            long rtnum1 = rs.getLong("rtnum1");
            long rtnum2 = rs.getLong("rtnum2");
            float rtflt1 = rs.getFloat("rtflt1");
            float rtflt2 = rs.getFloat("rtflt2");

            DTCPolicy p = new DTCPolicy(
                    polcod, polval, polvar, srtseq, rtstr1, rtstr2, rtnum1,
                    rtnum2, rtflt1, rtflt2
            );

            pMap.put(polval + ":" + srtseq, p);
        }

        return pMap;
    }

    public static long getSleepTime() {
        int minutes = Calendar.getInstance().get(Calendar.MINUTE);
        return (minutes < 45) ? (45 - minutes) * 60 * 1000 : (60 - minutes) * 60 * 1000;
    }
}
