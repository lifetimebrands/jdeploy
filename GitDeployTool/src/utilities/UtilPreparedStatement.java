package utilities;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilPreparedStatement {

	String origSql;
	String translatedSql;
	String parameters[];
	boolean translated = false;
	
	//PreparedStatement utilStatement;

	public UtilPreparedStatement(String sql) {
		origSql = sql;
		translatedSql = sql;
		//utilStatement = c.prepareStatement(origSql);
			                
		int idx = 0;
		for( int i = 0; i < origSql.length(); i++ ) {
			String s = origSql.substring(i, i+1);			
			if( s.equals("?") ) idx ++;
		}
						
		parameters = new String[idx];
	}

	public String getTranslatedSql() {
		
		try {
			if( !translated ) {
				int x = -1;
				for( int i = 0; i < parameters.length; i ++ ) {
					
					if(parameters[i]==null) {
						DaUtilities.writeMessage(					
								"UtilPreparedStatement.getTranslatedSql: parameter " + (i+1) + " is not set!", 
								true);
					}
					x = translatedSql.indexOf("?", x + 1);
				
					String firstHalf = translatedSql.substring(0, x);
					String secondHalf = translatedSql.substring(x + 1);
				
					translatedSql = firstHalf + parameters[i] + secondHalf;
					x += parameters[i].length();			
				}
				
				translated = true;
			}
		}
		catch(Exception ex) {
			DaUtilities.writeMessage("UtilPreparedStatement.getTranslatedSql: " + ex.toString(), true);
		}
		return translatedSql;
	}

	public void setString(int pos, String val) {

		////utilStatement.setString(pos, val);		
					
		try {
			if (val == null || val.length() == 0) {
				translate(pos, "NULL");
			} else {
								
				val = val.replace("'", "''");				
				translate(pos, "'" + val + "'");
			}
		} catch (Exception e) {
			translate(pos, "NULL");
		}
	}

	public void setTimestamp(int pos, Date updateDate) {
		
		if( updateDate == null ) {
			translate(pos, "NULL");
		}
		else {
			String format = "MM/dd/yyyy HH:mm:ss";
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			String fDate = sdf.format(updateDate);
			StringBuilder val = new StringBuilder("to_date('");
			val.append(fDate);
			val.append("', 'MM/DD/YYYY HH24:MI:SS')");			
			translate(pos, val.toString());
		}
		
		//utilStatement.setTimestamp(pos, new java.sql.Timestamp(updateDate
		//.getTime()));
	}

	public void setDate(int pos, Date updateDate) {
		
		if( updateDate == null ) {
			translate( pos, "NULL" );
		}
		else {
			String format = "MM/dd/yyyy HH:mm:ss";
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			String fDate = sdf.format(updateDate);
			StringBuilder val = new StringBuilder("to_date('");
			val.append(fDate);
			val.append("', 'MM/DD/YYYY HH24:MI:SS')");
			translate(pos, val.toString());
		}

		//utilStatement.setTimestamp(pos, new java.sql.Timestamp(updateDate
				//.getTime()));		
	}

	public void setLong(int pos, long l) {
				
		//utilStatement.setLong(pos, l);
		translate(pos, Long.toString(l));
	}
	
	public void setInt(int pos, int i) {
		
		//utilStatement.setInt(pos, i);
		translate(pos, Integer.toString(i));
	}
	
	public void setFloat(int pos, float f) {
		
		//utilStatement.setFloat(pos, f);
		translate(pos, Float.toString(f));
	}
	
	//public int executeUpdate() throws SQLException {		
		//return //utilStatement.executeUpdate();
	//}

	//public ResultSet executeQuery() throws SQLException {
		//return //utilStatement.executeQuery();
	//}
	
	public void close() throws SQLException {
		//utilStatement.close();
	}

	public void reset() {
		//utilStatement.clearParameters();
		translatedSql = origSql;
		translated = false;
	}

	private void translate(int pos, String val) {
				
		parameters[pos-1] = val;		
	}
}
