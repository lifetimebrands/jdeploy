package gitdeploytool;

/**
 *
 * @author eliotm
 */
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableRow;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.eclipse.jgit.api.PullResult;
import utilities.DaUtilities;
import utilities.QueriesEnum;
import utilities.UtilPreparedStatement;

public class GitDeployTool extends Application {

    final TreeItem<DisplayEntity> root
            = new TreeItem<>(new DisplayEntity("Ready To Roll In Branches", "", "", "", ""));

    final Button deployButton;
    final Button rollBackButton;
    String userID;
    VBox statusBar = new VBox();
    final Text statusText = new Text("Playing Game");

    
    public static void main(String[] args) {
        Application.launch(GitDeployTool.class, args);
    }

    public GitDeployTool() {
        this.deployButton = new Button("Deploy");
        this.rollBackButton = new Button("Roll Back");
    }

    private short loginUser() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("User Login");
        dialog.setHeaderText("Please Login to the Deploy Tool.");
        dialog.setResizable(false);

        Label label1 = new Label("User: ");
        Label label2 = new Label("Password: ");
        TextField loginTextField = new TextField();
        final PasswordField passwordFld = new PasswordField();

        GridPane grid = new GridPane();
        grid.add(label1, 1, 1);
        grid.add(loginTextField, 2, 1);
        grid.add(label2, 1, 2);
        grid.add(passwordFld, 2, 2);
        grid.setHgap(5);
        grid.setVgap(5);
        dialog.getDialogPane().setContent(grid);

        ButtonType buttonTypeOk = new ButtonType("Okay", ButtonData.OK_DONE);        
        dialog.getDialogPane().getButtonTypes().add(buttonTypeOk);
        Platform.runLater(() -> {
            loginTextField.requestFocus();
        });
        
        try {            
            Optional<ButtonType> result = dialog.showAndWait();
            
            if (result.get() == buttonTypeOk) {
                try (Connection dbConn = DaUtilities.getOracleConnection("PROD")) {
                    UtilPreparedStatement checkLoginStatement = new UtilPreparedStatement(
                            QueriesEnum.USR_LOGIN.getQuery());

                    checkLoginStatement.setString(1, loginTextField.getText().toUpperCase());
                    checkLoginStatement.setString(2, passwordFld.getText().toUpperCase());

                    try (ResultSet rs = DaUtilities.runQuery(dbConn, checkLoginStatement, false)) {
                        rs.next();                        
                        String usr = rs.getString("usr_id").toUpperCase();
                        String sts = rs.getString("usr_sts").toUpperCase();
                        
                        if(sts.equals("A")) {
                            userID = usr;
                            return 1;
                        }
                        else {
                            return -1;
                        }
                    }
                } catch(Exception ex) {
                    System.out.println(ex);
                    return -1;
                }                
            } else {
                return -99;
            }
        } catch (Exception ex) {
            return -99;
        }
    }

    private void loadLTBBranches() throws Exception {

        DaUtilities.writeMessage("GitDeployTool.loadLTBBranches()", false);
        List<String> jiraList = JIRAKey.getJIRAKeysInStatus(JIRAKey.DEPLOY_STATUS);
        int i = 0;
        for (String s : jiraList) {
            i++;
            DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.loadLTBBranches: finding evironment for key {0}", s), false);

            List<LTBBranch> branchList = CodeEnvironment.findEnvironmentsForJIRAKey(s);
            int j = 0;
            for (LTBBranch ltb : branchList) {
                ++j;

                if (ltb.checkIsMasterBehind()) {
                    DaUtilities.writeMessage("***** Master is Behind, Syncing it up . . .", false);
                    pullMasterForBranch(ltb);
                    ltb.checkIsMasterBehind();
                }

                String status;
                if (!ltb.isIsInDB()) {
                    status = "";
                } else if (ltb.isIsRolledIn()) {
                    status = "ROLLED IN";
                } else {
                    status = "ROLLED OUT";
                }

                String sTag = MessageFormat.format("{0} ::({1})", s, -ltb.getMasterBehindCount());
                DaUtilities.writeMessage("---- GitDeployTool.loadLTBBranches: Creating Tree Item", false);
                TreeItem<DisplayEntity> ti = new TreeItem<>(new DisplayEntity(sTag,
                        ltb.getLocalorRemoteTag(), ltb.getEnvDescription(), ltb.getBranchName(),
                        status));

                DaUtilities.writeMessage("---- GitDeployTool.loadLTBBranches: Getting Changes", false);
                List<LTBFile> ltbFiles = ltb.getBranchFileChanges();
                for (LTBFile file : ltbFiles) {
                    DisplayEntity ltbb = new DisplayEntity(sTag, file.getModType(),
                            file.getFileName(), file.getFullPath(), "");

                    DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.loadLTBBranches: Loading Tree Item with {0}", file.getFileName()), false);
                    ti.getChildren().add(new TreeItem<>(ltbb));
                }

                root.getChildren().add(ti);
            }
        }
    }

    private void setupButtons(TreeTableView<DisplayEntity> ttv) {
        DaUtilities.writeMessage("GitDeployTool.setupButtons(ttv)", false);

        deployButton.setOnAction((ActionEvent e) -> {
                        
            DisplayEntity commit = ttv.getSelectionModel().getSelectedItem().getValue();
            String jiraTag = (commit.getJira().indexOf("::(") > 0)
                    ? commit.getJira().substring(0, commit.getJira().indexOf("::(")).trim()
                    : commit.getJira().trim();
            String env = commit.getFileName();

            if (!commit.getFileAction().equals("LOCAL")) {
                Alert exAlert = new Alert(AlertType.ERROR);
                exAlert.setTitle("Error");
                exAlert.setHeaderText("Deployment Error");
                exAlert.setContentText("Only local branches can be deployed!!!");
                exAlert.showAndWait();
            } else {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Confirmation - (" + userID + ")");
                alert.setHeaderText("Confirm Deployment");
                alert.setContentText("Do you want Deploy Branch: "
                        + jiraTag);
                Optional<ButtonType> result = alert.showAndWait();

                if (result.get() == ButtonType.OK) {

                    DaUtilities.writeMessage("---- GitDeployTool.setupButtons: Deploying", false);
                    try {
                        LTBBranch ltb = CodeEnvironment.findEvironmentForJIRAKey(jiraTag, env);

                        if (ltb.checkIsMasterBehind()) {
                            DaUtilities.writeMessage("---- GitDeployTool.setupButtons: Deployment Master Branch is not current, fail", false);
                            Alert exAlert = new Alert(AlertType.WARNING);
                            exAlert.setTitle("Warning");
                            exAlert.setHeaderText("The Master Branch is behind the remote repository, please sync.");
                            exAlert.showAndWait();
                        } else {
                            HashMap<Boolean, List<String>> hm = ltb.rollinBranch(userID);

                            boolean aOK = hm.keySet().iterator().next();
                            StringBuilder msgString = new StringBuilder();
                            hm.get(aOK).stream().forEach((s) -> {
                                msgString.append(s).append("\n");
                            });

                            TextArea ta = new TextArea(msgString.toString());
                            ta.setEditable(false);

                            if (aOK) {

                                DaUtilities.writeMessage("---- GitDeployTool.setupButtons: Deployment Successful", false);
                                Alert exAlert = new Alert(AlertType.INFORMATION);
                                exAlert.setTitle("Success");
                                exAlert.setHeaderText("Branch was successfully deployed!");
                                exAlert.getDialogPane().setContent(ta);
                                exAlert.getDialogPane().setPrefSize(600, 800);
                                exAlert.setResizable(true);
                                exAlert.showAndWait();
                            } else {
                                DaUtilities.writeMessage("---- GitDeployTool.setupButtons: Deployment returned false", false);
                                Alert exAlert = new Alert(AlertType.WARNING);
                                exAlert.setTitle("Warning");
                                exAlert.setHeaderText("There was a deployment issue, hash mismatch, partial deployment possible.  Please review the log!");
                                exAlert.getDialogPane().setContent(ta);
                                exAlert.getDialogPane().setPrefSize(600, 800);
                                exAlert.setResizable(true);
                                exAlert.showAndWait();
                            }
                        }
                    } catch (Exception ex) {
                        DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.setupButtons: Deployment error \n {0}", ex), false);
                        Alert exAlert = new Alert(AlertType.ERROR);
                        exAlert.setTitle("Error");
                        exAlert.setHeaderText("Deployment Error");
                        exAlert.setContentText(ex.toString());
                        exAlert.showAndWait();
                    }
                }
            }

            refreshTableData();
        });

        rollBackButton.setOnAction((ActionEvent e) -> {
            DisplayEntity commit = ttv.getSelectionModel().getSelectedItem().getValue();
            String jiraTag = (commit.getJira().indexOf("::(") > 0)
                    ? commit.getJira().substring(0, commit.getJira().indexOf("::(")).trim()
                    : commit.getJira().trim();
            String env = commit.getFileName();

            if (!commit.getFileAction().equals("LOCAL")) {
                Alert exAlert = new Alert(AlertType.ERROR);
                exAlert.setTitle("Error");
                exAlert.setHeaderText("Deployment Error");
                exAlert.setContentText("Only local branches can be rolled back!!!");
                exAlert.showAndWait();
            } else {
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Confirmation - (" + userID + ")");
                alert.setHeaderText("Confirm Roll Back");
                alert.setContentText("Do you want Roll Back Branch: "
                        + jiraTag);
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    DaUtilities.writeMessage("---- GitDeployTool.setupButtons: RollBack", false);

                    try {

                        LTBBranch ltb = CodeEnvironment.findEvironmentForJIRAKey(jiraTag, env);

                        if (ltb.checkIsMasterBehind()) {
                            DaUtilities.writeMessage("---- GitDeployTool.setupButtons: Rollback Master Branch is not current, fail", false);
                            Alert exAlert = new Alert(AlertType.WARNING);
                            exAlert.setTitle("Warning");
                            exAlert.setHeaderText("The Master Branch is behind the remote repository, please sync.");
                            exAlert.showAndWait();
                        } else {

                            HashMap<Boolean, List<String>> hm = ltb.rollbackBranch(userID);

                            boolean aOK = hm.keySet().iterator().next();
                            StringBuilder msgString = new StringBuilder();
                            hm.get(aOK).stream().forEach((s) -> {
                                msgString.append(s).append("\n");
                            });

                            TextArea ta = new TextArea(msgString.toString());
                            ta.setEditable(false);

                            if (aOK) {
                                DaUtilities.writeMessage("---- GitDeployTool.setupButtons: Rollback Successful", false);
                                Alert exAlert = new Alert(AlertType.INFORMATION);
                                exAlert.setTitle("Success");
                                exAlert.setHeaderText("Branch was successfully rolled back!");
                                exAlert.getDialogPane().setContent(ta);
                                exAlert.getDialogPane().setPrefSize(600, 800);
                                exAlert.setResizable(true);
                                exAlert.showAndWait();
                            } else {
                                DaUtilities.writeMessage("---- GitDeployTool.setupButtons: Rollback returned false", false);
                                Alert exAlert = new Alert(AlertType.WARNING);
                                exAlert.setTitle("Warning");
                                exAlert.setHeaderText("There was a rollback issue, hash mismatch, partial rollback possible.  Please review the log!");
                                exAlert.getDialogPane().setContent(ta);
                                exAlert.getDialogPane().setPrefSize(600, 800);
                                exAlert.getDialogPane();
                                exAlert.setResizable(true);
                                exAlert.showAndWait();
                            }
                        }
                    } catch (Exception ex) {
                        DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.setupButtons: Rollback error \n {0}", ex), false);
                        Alert exAlert = new Alert(AlertType.ERROR);
                        exAlert.setTitle("Error");
                        exAlert.setHeaderText("Deployment Error");
                        exAlert.setContentText(ex.toString());
                        exAlert.showAndWait();
                    }
                }
            }

            refreshTableData();
        });
    }

    private void startLogging() {

        Properties props = null;
        try {
            props = DaUtilities.getProperties("gitdeploytool/GitDeployToolProperties");
        } catch (IOException ex) {
            Alert exAlert = new Alert(AlertType.ERROR);
            exAlert.setTitle("Error");
            exAlert.setHeaderText("Initialization Error");
            exAlert.setContentText(ex.toString());
            exAlert.showAndWait();

            System.exit(0);
        }

        if (props != null && props.getProperty("LOGMETHOD").equals("FILE")) {

            Calendar c;
            PrintStream ps;
            String logDir = props.getProperty("LOG_DIR");
            String logFile = null;

            c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String s = sdf.format(c.getTime());
            FileOutputStream fos = null;

            logFile = s;

            try {
                fos = new FileOutputStream(logDir + "/DeployTool-" + logFile + ".txt", true);
            } catch (IOException iox) {
                DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.startLogging\n{0}", iox.toString()), true);

                Alert exAlert = new Alert(AlertType.ERROR);
                exAlert.setTitle("Error");
                exAlert.setHeaderText("Initialization Error");
                exAlert.setContentText(iox.toString());
                exAlert.showAndWait();

                System.exit(0);
            }

            ps = new PrintStream(fos);
            System.setOut(ps);
            System.setErr(ps);
        } else if (props == null) {
            Alert exAlert = new Alert(AlertType.ERROR);
            exAlert.setTitle("Error");
            exAlert.setHeaderText("Initialization Error");
            exAlert.setContentText("Unable to establish Logging!");
            exAlert.showAndWait();

            System.exit(0);
        }

        DaUtilities.writeMessage("GitDeployTool.startLogging, Log Method: " + props.getProperty("LOGMETHOD"), false);
    }

    @Override
    public void start(Stage stage) {

        int failedCount = 0;
        boolean goodLogin = false;
        do {
            short result = loginUser();
            //short result = 1;
            DaUtilities.writeMessage("Login Result: " + result, false);
            if (result != 1) {

                if (result == -1) {
                    Alert badLoginAlert = new Alert(AlertType.ERROR);
                    badLoginAlert.setTitle("Error");
                    badLoginAlert.setHeaderText("Invalid Login Attempt!");
                    badLoginAlert.showAndWait();

                    ++failedCount;
                } else {
                    System.exit(0);
                }
            } else {
                goodLogin = true;
            }
        } while (failedCount < 3 && !goodLogin);

        if (!goodLogin) {
            Alert exAlert = new Alert(AlertType.ERROR);
            exAlert.setTitle("Error");
            exAlert.setHeaderText("Too Many Failed Logins, Exiting . . .");
            exAlert.showAndWait();

            System.exit(0);
        }

        DaUtilities.writeMessage("Logged in user: " + userID, false);        
        statusBar.setStyle("-fx-background-color: gainsboro");   
        statusBar.getChildren().add(statusText);
        statusText.setText("User: " + userID);
        
        startLogging();

        DaUtilities.writeMessage("GitDeployTool.start(Stage)", false);
        DaUtilities.writeMessage("---- GitDeployTool.start: LoadingBranches", false);

        refreshTableData();

        root.setExpanded(true);

        stage.setTitle("JIRA Deployment Tool");

        TreeTableColumn<DisplayEntity, String> branchColumn;
        branchColumn = new TreeTableColumn<>("JIRA");
        branchColumn.setPrefWidth(200);
        branchColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DisplayEntity, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getJira())
        );

        branchColumn.setCellFactory(column -> {
            return new TreeTableCell<DisplayEntity, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        if (item.contains("::(-")) {
                            setTextFill(Color.RED);
                            setStyle("");
                        } else {
                            setTextFill(Color.DARKGREEN);
                            setStyle("");
                        }

                        setText((item.indexOf("::(") > 0) ? item.substring(0, item.indexOf("::(")) : item);
                    }
                }
            };
        });

        TreeTableColumn<DisplayEntity, String> actionColumn;
        actionColumn = new TreeTableColumn<>("Action");
        actionColumn.setPrefWidth(100);
        actionColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DisplayEntity, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getFileAction())
        );

        actionColumn.setCellFactory(column -> {
            return new TreeTableCell<DisplayEntity, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        //System.out.println(item);
                        switch (item) {
                            case "LOCAL":
                                setTextFill(Color.DARKGREEN);
                                setStyle("");
                                break;
                            case "REMOTE":
                                setTextFill(Color.DARKORANGE);
                                setStyle("");
                                break;
                            default:
                                setTextFill(Color.BLACK);
                                setStyle("");
                                break;
                        }

                        setText(item);
                    }
                }
            };
        });

        TreeTableColumn<DisplayEntity, String> fileNameColumn;
        fileNameColumn = new TreeTableColumn<>("File Name");
        fileNameColumn.setPrefWidth(300);
        fileNameColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DisplayEntity, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getFileName())
        );

        TreeTableColumn<DisplayEntity, String> locationColumn
                = new TreeTableColumn<>("Location");
        locationColumn.setPrefWidth(480);
        locationColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DisplayEntity, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getLocation())
        );

        TreeTableColumn<DisplayEntity, String> statusColumn
                = new TreeTableColumn<>("Status");
        statusColumn.setPrefWidth(200);
        statusColumn.setCellValueFactory(
                (TreeTableColumn.CellDataFeatures<DisplayEntity, String> param)
                -> new ReadOnlyStringWrapper(param.getValue().getValue().getStatus())
        );

        statusColumn.setCellFactory(column -> {
            return new TreeTableCell<DisplayEntity, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    TreeTableRow<DisplayEntity> ttr = getTreeTableRow();

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                        ttr.setStyle("");
                    } else {
                        //System.out.println(item);
                        switch (item) {
                            case "ROLLED IN":
                                setTextFill(Color.DARKGREEN);
                                ttr.setStyle("-fx-background-color:lightgreen");
                                setStyle("");
                                break;
                            case "ROLLED OUT":
                                setTextFill(Color.RED);
                                ttr.setStyle("-fx-background-color:orange");
                                setStyle("");
                                break;
                            default:
                                ttr.setStyle("");
                                setTextFill(Color.BLACK);
                                setStyle("");
                                break;
                        }

                        setText(item);
                    }
                }
            };
        });

        TreeTableView<DisplayEntity> treeTableView = new TreeTableView<>(root);
        treeTableView.getColumns().setAll(branchColumn, actionColumn, fileNameColumn, locationColumn, statusColumn);
        treeTableView.setPrefHeight(600);

        treeTableView.setRowFactory(ttv -> {
            ContextMenu contextMenu = new ContextMenu();
            MenuItem refreshMenuItem = new MenuItem("Refresh Table");
            MenuItem checkoutMenuItem = new MenuItem("Checkout Branch");
            MenuItem closeJIRAMenuItem = new MenuItem("Close JIRA");

            contextMenu.getItems().add(refreshMenuItem);
            contextMenu.getItems().add(checkoutMenuItem);
            contextMenu.getItems().add(closeJIRAMenuItem);

            TreeTableRow<DisplayEntity> row = new TreeTableRow<DisplayEntity>() {
                @Override
                public void updateItem(DisplayEntity item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setContextMenu(null);
                    } else {
                        setContextMenu(contextMenu);
                    }
                }
            };

            closeJIRAMenuItem.setOnAction(evt -> {
                DisplayEntity item = row.getItem();
                closeJIRA(item);
            });

            checkoutMenuItem.setOnAction(evt -> {
                DisplayEntity item = row.getItem();
                checkoutRemoteBranch(item);
            });

            refreshMenuItem.setOnAction(evt -> {
                refreshTableData();
            });

            return row;
        });

        DaUtilities.writeMessage("---- GitDeployTool.start: setting up buttons", false);
        setupButtons(treeTableView);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        GridPane buttonPane = new GridPane();
        buttonPane.setAlignment(Pos.CENTER);
        buttonPane.setHgap(10);
        buttonPane.setVgap(10);
        buttonPane.setPadding(new Insets(25, 25, 25, 25));

        buttonPane.add(deployButton, 0, 0);
        buttonPane.add(rollBackButton, 1, 0);

        BorderPane bp = new BorderPane();
        bp.setCenter(grid);
        bp.setBottom(statusBar);
        
        final Scene scene = new Scene(bp, 1280, 720);
        scene.setFill(Color.LIGHTGRAY);
        //Group sceneRoot = (Group) scene.getRoot();

        grid.add(treeTableView, 0, 0);
        grid.add(buttonPane, 0, 1);

        DaUtilities.writeMessage("---- GitDeployTool.start: Displaying Form", false);
        stage.setScene(scene);
        stage.show();
    }

    private void closeJIRA(DisplayEntity de) {

        String jiraTag = (de.getJira().indexOf("::(") > 0)
                ? de.getJira().substring(0, de.getJira().indexOf("::(")).trim()
                : de.getJira().trim();

        if (!de.getStatus().equals("ROLLED IN")) {
            Alert exAlert = new Alert(AlertType.ERROR);
            exAlert.setTitle("Error");
            exAlert.setHeaderText("Close JIRA Error");
            exAlert.setContentText("The JIRA is not ROLLED IN!!!");
            exAlert.showAndWait();
        } else {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirm JIRA Close");
            alert.setContentText(MessageFormat.format("Do you want close this JIRA : {0}?", jiraTag));
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {

                int rez = 0;
                try {
                    rez = JIRAKey.closeJIRA(jiraTag);

                } catch (Exception ex) {
                    Logger.getLogger(GitDeployTool.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

                DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.closeJIRA: Close JIRA Result {0}", rez), false);
            }
        }

        refreshTableData();

    }

    private void pullMasterForBranch(LTBBranch ltb) {
        DaUtilities.writeMessage(MessageFormat.format("GitDeployTool.pullMasterForBranch({0})", ltb.getBranchName()), false);

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Pull Master");
        alert.setContentText(MessageFormat.format("The Environment ({0}) is behind, do you want to pull now?", ltb.getEnvDescription()));
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            try {
                if (!ltb.isMasterBehind()) {
                    alert = new Alert(AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Master is not behind remote!");
                    alert.showAndWait();
                } else {
                    PullResult pr = ltb.pullMaster();
                    alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Complete");
                    alert.setHeaderText("Pull Master");
                    alert.setContentText(MessageFormat.format("Result {0}\n{1}\n\n{2}",
                            (pr.isSuccessful()) ? "Success" : "Fail",
                            pr.getFetchResult().getMessages(),
                            "NOTE: In Source Tree you may have to refresh remote to see effect of pull (Repository : Refresh Remote Status)!"));
                    alert.showAndWait();
                }
            } catch (Exception ex) {
                DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.pullMasterForBranch\n{0}", ex), false);
                alert = new Alert(AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(ex.toString());
                alert.showAndWait();
            }
        }
    }

    private void checkoutRemoteBranch(DisplayEntity de) {
        DaUtilities.writeMessage(MessageFormat.format("GitDeployTool.checkoutBranch({0})", de.getJira()), false);

        String jiraTag = (de.getJira().indexOf("::(") > 0)
                ? de.getJira().substring(0, de.getJira().indexOf("::(")).trim()
                : de.getJira().trim();
        String env = de.getFileName();

        if (!de.getFileAction().equals("REMOTE")) {
            Alert exAlert = new Alert(AlertType.ERROR);
            exAlert.setTitle("Error");
            exAlert.setHeaderText("Checkout Error");
            exAlert.setContentText("Only remote branches can be checked out!!!");
            exAlert.showAndWait();
        } else {

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Confirm Checkout");
            alert.setContentText("Do you want Checkout Branch for : "
                    + jiraTag);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                    LTBBranch ltb = CodeEnvironment.findRemoteHeadForJIRAKey(jiraTag, env);
                    if (ltb.checkIsMasterBehind()) {
                        pullMasterForBranch(ltb);
                        ltb = CodeEnvironment.findRemoteHeadForJIRAKey(jiraTag, env);

                        if (ltb.checkIsMasterBehind()) {
                            Alert exAlert = new Alert(AlertType.ERROR);
                            exAlert.setTitle("Error");
                            exAlert.setHeaderText("Checkout Error");
                            exAlert.setContentText("Master must not be behind to pull remote branch");
                            exAlert.showAndWait();
                        }
                    }

                    boolean aOK = ltb.checkoutBranch();

                    if (!aOK) {
                        Alert exAlert = new Alert(AlertType.ERROR);
                        exAlert.setTitle("Error");
                        exAlert.setHeaderText("Checkout Error");
                        exAlert.setContentText("Branch Checkout Failed, see log for details");
                        exAlert.showAndWait();
                    }

                    refreshTableData();

                    Alert exAlert = new Alert(AlertType.INFORMATION);
                    exAlert.setTitle("Error");
                    exAlert.setHeaderText("Checkout Complete");
                    exAlert.setContentText("Branch Checkout Complete\n\n"
                            + "In external tools your current branch may be set to this branch!");
                    exAlert.showAndWait();
                } catch (Exception ex) {
                    DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.checkoutBranch: {0}", ex), true);
                    Alert exAlert = new Alert(AlertType.ERROR);
                    exAlert.setTitle("Error");
                    exAlert.setHeaderText("Checkout Error");
                    exAlert.setContentText(ex.getMessage());
                    exAlert.showAndWait();
                }
            }
        }
    }

    private void refreshTableData() {
        DaUtilities.writeMessage("GitDeployTool.refreshTableData()", false);

        try {
            root.getChildren().clear();
            loadLTBBranches();
        } catch (Exception ex) {
            DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.refreshTableData\n{0}", ex), false);
        }
    }

    public class DisplayEntity {

        private SimpleStringProperty jira;
        private SimpleStringProperty fileAction;
        private SimpleStringProperty fileName;
        private SimpleStringProperty location;
        private SimpleStringProperty status;

        public SimpleStringProperty fileActionProperty() {
            if (fileAction == null) {
                fileAction = new SimpleStringProperty(this, "N/A");
            }
            return fileAction;
        }

        public SimpleStringProperty jiraProperty() {
            if (jira == null) {
                jira = new SimpleStringProperty(this, "N/A");
            }
            return jira;
        }

        public SimpleStringProperty fileNameProperty() {
            if (fileName == null) {
                fileName = new SimpleStringProperty(this, "N/A");
            }
            return fileName;
        }

        public SimpleStringProperty locationProperty() {
            if (location == null) {
                location = new SimpleStringProperty(this, "N/A");
            }
            return location;
        }

        public SimpleStringProperty statusProperty() {
            if (status == null) {
                status = new SimpleStringProperty(this, "N/A");
            }
            return status;
        }

        private DisplayEntity(String jira, String fileAction, String fileName, String location, String status) {
            this.jira = new SimpleStringProperty(jira);
            this.fileAction = new SimpleStringProperty(fileAction);
            this.fileName = new SimpleStringProperty(fileName);
            this.location = new SimpleStringProperty(location);
            this.status = new SimpleStringProperty(status);
        }

        public String getJira() {
            return jira.get();
        }

        public void setJira(String fName) {
            jira.set(fName);
        }

        public String getFileAction() {
            return fileAction.get();
        }

        public void setFileAction(String fName) {
            fileAction.set(fName);
        }

        public String getFileName() {
            return fileName.get();
        }

        public void setFileName(String fName) {
            fileName.set(fName);
        }

        public String getLocation() {
            return location.get();
        }

        public void setLocation(String fName) {
            location.set(fName);
        }

        public String getStatus() {
            return status.get();
        }

        public void setStatus(String fName) {
            status.set(fName);
        }
    }
}
