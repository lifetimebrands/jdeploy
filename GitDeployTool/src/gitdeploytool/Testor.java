/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitdeploytool;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import org.eclipse.jgit.api.PullResult;
import utilities.DaUtilities;

/**
 *
 * @author eliotm
 */
public class Testor {

    public static void main(String args[]) throws Exception {
        List<String> jiraList = JIRAKey.getJIRAKeysInStatus(JIRAKey.DEPLOY_STATUS);
        for(String s : jiraList) {
            System.out.println("BranchName: " + s);
            List<LTBBranch> branchList = CodeEnvironment.findEnvironmentsForJIRAKey(s);
            System.out.println("Size, " + branchList.size());
            int j = 0;
            for (LTBBranch ltb : branchList) {
                System.out.println("IN OTHER LOOP . . .");
                ++j;

                if (ltb.checkIsMasterBehind()) {
                    DaUtilities.writeMessage("***** Master is Behind, Syncing it up . . .", false);  
                    pullMasterForBranch(ltb);
                    ltb.checkIsMasterBehind();
                }

                String status;
                if (!ltb.isIsInDB()) {
                    status = "";
                } else if (ltb.isIsRolledIn()) {
                    status = "ROLLED IN";
                } else {
                    status = "ROLLED OUT";
                }
            }
        }
    }
    
     private static void pullMasterForBranch(LTBBranch ltb) {
        DaUtilities.writeMessage(MessageFormat.format("GitDeployTool.pullMasterForBranch({0})", ltb.getBranchName()), false);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Pull Master");
        alert.setContentText(MessageFormat.format("The Environment ({0}) is behind, do you want to pull now?", ltb.getEnvDescription()));
        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            try {
                if (!ltb.isMasterBehind()) {
                    alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Master is not behind remote!");
                    alert.showAndWait();
                } else {
                    PullResult pr = ltb.pullMaster();
                    alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Complete");
                    alert.setHeaderText("Pull Master");
                    alert.setContentText(MessageFormat.format("Result {0}\n{1}\n\n{2}",
                            (pr.isSuccessful()) ? "Success" : "Fail",
                            pr.getFetchResult().getMessages(),
                            "NOTE: In Source Tree you may have to refresh remote to see effect of pull (Repository : Refresh Remote Status)!"));
                    alert.showAndWait();
                }
            } catch (Exception ex) {
                DaUtilities.writeMessage(MessageFormat.format("---- GitDeployTool.pullMasterForBranch\n{0}", ex), false);
                alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText(ex.toString());
                alert.showAndWait();
            }
        }
    }
}
