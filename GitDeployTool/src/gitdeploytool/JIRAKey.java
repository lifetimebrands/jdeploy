package gitdeploytool;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import utilities.DaUtilities;

/**
 *
 * @author eliotm
 */
public class JIRAKey {

    public final static String QA_STATUS = "\"In%20Testing%20(QA)\"";        
    public final static String DEPLOY_STATUS = "Building";
    public final static String DEPLOYED = "\"Project%20Deployed\"";
    public final static String CLOSE_ID = "71";
    public final static String REOPEN_ID = "81";
    
    private static final String JIRA_BASE_HTTP = "https://njrobmis.atlassian.net/rest/api/latest/";
    
    private String keyName;
    
    public JIRAKey (String keyName) {
        this.keyName = keyName;                
    }
    
    public String getKeyName() {
        return keyName;
    }
    
    public void setKeyName(String kn) {
        keyName = kn;
    }
    
    /**
     *
     * @param st
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public final static List<String> getJIRAKeysInStatus(String st) throws IOException, ParseException {
        DaUtilities.writeMessage(MessageFormat.format("JIRAKey.getJIRAKeysInStatus({0})", st), false);
        
        URL url = new URL(MessageFormat.format("{0}search?jql=status%20=%20{1}", JIRA_BASE_HTTP, st));
        URLConnection uc = url.openConnection();
        String basicAuth = "Basic " + "a2VpdGgucmlsbDpNeWxpdnliZW4xMw==";
        uc.setRequestProperty("Authorization", basicAuth);

        DaUtilities.writeMessage("---- JIRAKey.getJIRAKeysInStatus: Checking JIRA Site . . .", false);
        
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));

        DaUtilities.writeMessage("---- JIRAKey.getJIRAKeysInStatus: Parsing results", false);
        
        JSONParser jsonParser = new JSONParser();       
        JSONObject jsonObject = (JSONObject) jsonParser.parse(in);

        JSONArray lang = (JSONArray) jsonObject.get("issues");
        ArrayList<String> keys = new ArrayList<>();
        for (int i = 0; i < lang.size(); i++) {
            JSONObject j = (JSONObject) lang.get(i);
            keys.add( j.get("key").toString() );
        }
        
        DaUtilities.writeMessage(MessageFormat.format("---- JIRAKey.getJIRAKeysInStatus: returning keys {0}", keys.size()), false);
        
        return keys;
    }
    
    public static int closeJIRA(String jiraKey) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("JIRAKey.closeJIRA({0})", jiraKey), false);
        URL url = new URL(MessageFormat.format("{0}issue/{1}/transitions?expand=transitions.fields", JIRA_BASE_HTTP, jiraKey));
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        String basicAuth = "Basic " + "a2VpdGgucmlsbDpNeWxpdnliZW4xMw==";
        con.setRequestProperty("Authorization", basicAuth);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        
        String postData = "{\"transition\": {\"id\": \"71\"}}\n";
        
        // Send post request
        con.setDoOutput(true);
        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.writeBytes(postData);
            wr.flush();
        }

        int retCode = con.getResponseCode();              
        
        return retCode;
    }
}
