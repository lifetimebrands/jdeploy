package utilities;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author eliotm
 */
public class JIRALogEntry implements Insertable {

    private final String jiraId;
    private String branchId;
    private String objectType;
    private String objectName;
    private String sourceDir;
    private String changeType;
    private String destinationDir;
    private String gitPath;
    private String backupDir;
    private String fileHashBitBucket;
    private String fileHashStagingDir;
    private String fileHashDestinationDir;
    private String fileHashOrignalFile;
    private String fileHashBackupFile;
    private String userId;
    private final String operationType;
    private final long procId;
    private Boolean stageResult;
    private Boolean backupResult;
    private Boolean rollinResult;
    private final List<String> eventLog;

    public JIRALogEntry(String jiraId, String operationType, String objectName, String usrID) {
        DaUtilities.writeMessage(MessageFormat.format("JIRALogEntry.constructor({0}, {1}, {2})", jiraId, operationType, objectName), false);
        
        this.jiraId = jiraId;
        this.objectName = objectName;
        this.operationType = operationType;
        this.procId = Calendar.getInstance().getTimeInMillis();
        this.userId = usrID;
        
        stageResult = null;
        backupResult = null;
        rollinResult = null;

        eventLog = new ArrayList<>();
        createLogEntry(MessageFormat.format("{0} - {1}: {2}", jiraId, operationType, objectName));
    }

    public void writeEventLogOutput(String path) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("JIRALogEntry.writeEventLogOutput({0})", path), false);
        
        String fileName = jiraId + "-" + operationType + ".log";
        Writer out = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(path + "/" + fileName), "UTF-8"));
        try {
            for (String s : eventLog) {                
                out.write(s + "\n");
            }
        } finally {
            out.flush();
            out.close();
        }
    }

    public String getEventLogOutput() {
        
        StringBuilder sb = new StringBuilder();
        for (String s : eventLog) {
            sb.append(MessageFormat.format("{0}\n", s));
        }

        return sb.toString();
    }

    public List<String> getEventLog() {
       
        return eventLog;
    }

    public final void createLogEntry(String msg) {
        
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        eventLog.add(MessageFormat.format("{0}: {1}", ts, msg));
    }

    public boolean getStageResult() {
        DaUtilities.writeMessage("JIRALogEntry.getStageResult()", false);
        return stageResult;
    }

    public void setStageResult(boolean stageResult) {
        DaUtilities.writeMessage(MessageFormat.format("JIRALogEntry.setStageResult({0})", stageResult), false);
        this.stageResult = stageResult;
    }

    public boolean getBackupResult() {
        DaUtilities.writeMessage("JIRALogEntry.getBackupResult()", false);
        
        return backupResult;
    }

    public void setBackupResult(boolean backupResult) {
        DaUtilities.writeMessage(MessageFormat.format("JIRALogEntry.setBackupResult({0})", backupResult), false);
        
        this.backupResult = backupResult;
    }

    public boolean getRollinResult() {
        DaUtilities.writeMessage("JIRALogEntry.getRollinResult()", false);
        
        return rollinResult;
    }

    public void setRollinResult(boolean rollinResult) {
                
        this.rollinResult = rollinResult;
    }

    public String getJiraID() {
                
        return jiraId;
    }

    public String getBranchId() {
                
        return branchId;
    }

    public String getObjectType() {
                
        return objectType;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getSourceDir() {
        return sourceDir;
    }

    public String getBackupDir() {
        return backupDir;
    }

    public String getChangeType() {
        return changeType;
    }

    public String getDestinationDir() {
        return destinationDir;
    }

    public String getFileHashBitBucket() {
        return fileHashBitBucket;
    }

    public String getFileHashStagingDir() {
        return fileHashStagingDir;
    }

    public String getFileHashDestinationDir() {
        return fileHashDestinationDir;
    }

    public String getFileHashOriginalFile() {
        return fileHashOrignalFile;
    }

    public String getFileHashBackupFile() {
        return fileHashBackupFile;
    }

    public String getUserId() {
        return userId;
    }

    public String getOperationType() {
        return operationType;
    }

    public String getProcId() {
        return "" + procId + "-" + operationType;
    }

    public String getGitPath() {
        return gitPath;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public void setGitPath(String gitPath) {
        this.gitPath = gitPath;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public void setSourceDir(String sourceDir) {
        this.sourceDir = sourceDir;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    public void setDestinationDir(String destinationDir) {
        this.destinationDir = destinationDir;
    }

    public void setBackupDir(String backupDir) {
        this.backupDir = backupDir;
    }

    public void setFileHashBitBucket(String fileHashBitBucket) {
        this.fileHashBitBucket = fileHashBitBucket;
    }

    public void setFileHashStagingDir(String fileHashStagingDir) {
        this.fileHashStagingDir = fileHashStagingDir;
    }

    public void setFileHashDestinationDir(String fileHashDestinationDir) {
        this.fileHashDestinationDir = fileHashDestinationDir;
    }

    public void setFileHashOriginalFile(String fileHashOriginalFile) {
        this.fileHashOrignalFile = fileHashOriginalFile;
    }

    public void setFileHashBackupFile(String fileHashBackupFile) {
        this.fileHashBackupFile = fileHashBackupFile;
    }

    @Override
    public boolean insert(Connection conn) throws Exception {
        DaUtilities.writeMessage("JIRALogEntry.insert(Conn)", false);
        UtilPreparedStatement ups = new UtilPreparedStatement(
                QueriesEnum.INSERT_JIRA_LOG_ENTRY.getQuery());

        DaUtilities.writeMessage("---- JIRALogEntry.insert: setting up query", false);
        ups.setString(1, getJiraID());
        ups.setString(2, getBranchId());
        ups.setString(3, getObjectType());
        ups.setString(4, getObjectName());
        ups.setString(5, getSourceDir());
        ups.setString(6, getChangeType());
        ups.setString(7, getDestinationDir());
        ups.setString(8, getFileHashBitBucket());
        ups.setString(9, getFileHashStagingDir());
        ups.setString(10, getFileHashDestinationDir());
        ups.setString(11, getUserId());
        ups.setString(12, getOperationType());
        ups.setString(13, getFileHashOriginalFile());
        ups.setString(14, getFileHashBackupFile());
        ups.setString(15, getBackupDir());
        ups.setString(16, getGitPath());

        DaUtilities.writeMessage("---- JIRALogEntry.insert: executing query", false);
        return DaUtilities.executeUpdate(conn, ups, true);
    }
}
