package gitdeploytool;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.lib.BranchTrackingStatus;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.eclipse.jgit.util.io.DisabledOutputStream;
import utilities.DaUtilities;

/**
 * LTBRepository
 *
 * @author eliotm
 */
public class LTBRepository {

    private final Git git;
    private final Repository repository;
    private final RevCommit masterCommit;

    public LTBRepository(String repoPath) throws Exception {
        DaUtilities.writeMessage(MessageFormat.format("LTBRepository.Constructor({0})", repoPath), false);

        File gitWorkDir = new File(repoPath);
        git = Git.open(gitWorkDir);
        repository = git.getRepository();

        RevWalk revWalk = new RevWalk(repository);
        masterCommit = revWalk.parseCommit(repository.resolve("refs/heads/master"));
    }

    public PullResult pullMaster() throws Exception {
        try (Git fetchGit = new Git(repository)) {            
            return fetchGit.pull().call();
        }
    }

    public boolean checkoutBranch(String branchName) {
        DaUtilities.writeMessage(MessageFormat.format("LTBRepository.checkoutBranch({0})", branchName), false);

        try {
            pullMaster();
            
            git.checkout().
                    setCreateBranch(true).
                    setName(branchName).
                    setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK).
                    setStartPoint("origin/" + branchName).
                    call();
            
            git.pull().setRemoteBranchName(branchName).call();
        } catch (Exception ex) {
            DaUtilities.writeMessage(MessageFormat.format("---- LTBRepository.checkoutBranch ERROR:\n{0}", ex), false);
            return false;
        }

        return true;
    }

    public Ref getBranchRef(String s) throws Exception {
        DaUtilities.writeMessage("LTBRepository.getBranchRef()", false);

        return repository.exactRef(s);
    }

    public HashMap<String, Integer> getAheadBehindCounts(String branchName) throws IOException {
        HashMap<String, Integer> h = new HashMap<>();

        BranchTrackingStatus trackingStatus = BranchTrackingStatus.of(repository, branchName);
        if (trackingStatus != null) {
            h.put("AHEAD", trackingStatus.getAheadCount());
            h.put("BEHIND", trackingStatus.getBehindCount());
        } else {
            h.put("AHEAD", 0);
            h.put("BEHIND", 0);
        }

        return h;
    }

    public RevCommit getMasterCommit() {
        DaUtilities.writeMessage("LTBRepository.getMasterCommit()", false);

        return masterCommit;
    }

    public Iterable<RevCommit> rangeWalk(String branchId) throws Exception {
        DaUtilities.writeMessage("LTBRepository.rangeWalk(bID)", false);

        ObjectId master = repository.resolve("refs/heads/master");
        ObjectId branch = repository.resolve(branchId);

        return git.log().addRange(master, branch).call();
    }

    public RevCommit getCommit(String cId) throws IOException {
        DaUtilities.writeMessage("LTBRepository.getCommit(cID)", false);

        ObjectId commitId = ObjectId.fromString(cId);
        RevCommit revCommit;
        try (RevWalk revWalk = new RevWalk(repository)) {
            revCommit = revWalk.parseCommit(commitId);
        }

        return revCommit;
    }

    public List<DiffEntry> getCommitDiffs(RevCommit c1, RevCommit c2) throws IOException {
        DaUtilities.writeMessage("LTBRepository.getCommitDiffs(c1, c2)", false);

        try (DiffFormatter df = new DiffFormatter(DisabledOutputStream.INSTANCE)) {
            df.setRepository(repository);
            df.setDiffComparator(RawTextComparator.DEFAULT);
            df.setDetectRenames(true);

            return df.scan(c1.getTree(), c2.getTree());
        }
    }

    public boolean isMergedToMaster(RevCommit rc) throws Exception {
        DaUtilities.writeMessage("LTBRepository.isMergedToMaster(rc)", false);

        Ref head = repository.exactRef("refs/heads/master");
        if (head == null) {
            throw new Exception("LTBRepository.isMergedToMaster: Error refs/heads/master reference is null!!");
        }

        try (RevWalk walk = new RevWalk(repository)) {

            RevCommit commit = walk.parseCommit(head.getObjectId());
            walk.markStart(commit);

            boolean found = false;
            for (RevCommit rev : walk) {
                if (!found) {
                    if (rev.name().equals(rc.name())) {
                        found = true;
                    }
                }
            }

            walk.dispose();

            return found;
        }
    }

    public List<Ref> getBranches() throws GitAPIException {
        DaUtilities.writeMessage("LTBRepository.getBranches()", false);

        return git.branchList().call();
    }

    public Iterable<RevCommit> getAllCommits() throws IOException, GitAPIException {
        DaUtilities.writeMessage("LTBRepository.getAllCommits(commit, fId)", false);

        return git.log().all().call();
    }

    public ObjectLoader getFileLoader(RevCommit commit, String fId) throws Exception {
        DaUtilities.writeMessage("LTBRepository.getFileLoader(commit, fId)", false);

        RevTree tree = commit.getTree();

        TreeWalk treeWalk = new TreeWalk(repository);
        treeWalk.addTree(tree);
        treeWalk.setRecursive(true);
        treeWalk.setFilter(PathFilter.create(fId));

        if (!treeWalk.next()) {
            throw new IllegalStateException("Did not find expected file + '"
                    + fId + "' in " + commit.getName());
        }

        ObjectId objectId = treeWalk.getObjectId(0);
        ObjectLoader loader = repository.open(objectId);

        return loader;
    }

    public Collection<Ref> getRemoteBranches() throws Exception {
        DaUtilities.writeMessage("LTBRepository.getRemoteBranches()", false);

        try (Git rGit = new Git(repository)) {

            return rGit.lsRemote().setHeads(true).call();
        }
    }
}
