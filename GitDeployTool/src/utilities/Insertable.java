package utilities;

import java.sql.Connection;

public interface Insertable {

	public abstract boolean insert(Connection conn) throws Exception;
}
